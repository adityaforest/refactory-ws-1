import React from 'react'

function SubPage1() {
  return (
    <div className='d-flex  align-items-center'>
        <img className='mx-5' src="refactory-logo-2.png" alt="" style={{ width:'300px', height:'200px' }}/>
        <h1 className='mx-5'>This is Page 1</h1>
    </div>
  )
}

export default SubPage1