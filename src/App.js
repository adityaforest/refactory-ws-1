import './App.css';
import {BrowserRouter as Router,Routes,Route} from 'react-router-dom'
import Main from './pages/Main';
import Login from './pages/Login';
import Signup from './pages/Signup';
import SubPage1 from './pages/SubPage1';
import Subpage2 from './pages/Subpage2';

function App() {
  return (
    <div className='App'>          
      <Router>
        <Routes>
          <Route path='/' element={<Main/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/signup' element={<Signup/>}/>
          <Route path='/page1' element={<SubPage1/>}/>
          <Route path='/page2' element={<Subpage2/>}/>
        </Routes>
      </Router>      
    </div>
  );
}

export default App;
