import React from 'react'

function Navbar(props) {
    

    return (
        <div>            
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark p-2">
                <a className="navbar-brand" href="/"><img src="refactory-logo-1.png" style={{ width:'40px', height:'40px' }} /> Refactory</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className={window.location.pathname == '/' ? "nav-link text-white" : "nav-link"} href="/">Home</a>
                        </li>
                        <li className="nav-item">
                            <a className={window.location.pathname == '/page1' ? "nav-link text-white" : "nav-link"} href="/page1">Page 1</a>
                        </li>
                        <li className="nav-item">
                            <a className={window.location.pathname == '/page2' ? "nav-link text-white" : "nav-link"} href="/page2">Page 2</a>
                        </li>                        
                    </ul>
                    <ul className="navbar-nav ms-auto">
                        <li className="nav-item active">
                            <a className={window.location.pathname == '/login' ? "nav-link text-white" : "nav-link"} href="/login">Login</a>
                        </li>
                        <li className="nav-item">
                            <a className={window.location.pathname == '/signup' ? "nav-link text-white" : "nav-link"} href="/signup">Signup</a>
                        </li>                            
                    </ul>
                </div>
            </nav>
        </div>
    )
}

export default Navbar